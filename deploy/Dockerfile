FROM node:12-alpine as test
RUN apk add --no-cache git

ENV home /root
ENV app ${home}/app

COPY server/package*.json ${app}/server/
WORKDIR ${app}/server/
RUN npm ci

COPY server/ ${app}/server/

COPY client/package*.json ${app}/client/
WORKDIR ${app}/client/
RUN npm ci

COPY build-output/ ${app}/build-output/

COPY client/ ${app}/client/
WORKDIR ${app}/client/
RUN npm run lint
RUN npm run build

WORKDIR ${app}/server/
RUN npm run lint

FROM node:12-alpine as clean

ENV home /root
ENV app ${home}/app

COPY --from=test ${app} ${app}
WORKDIR ${app}/server/
RUN npm prune --production
RUN rm -rf ./coverage

WORKDIR ${app}/client
RUN rm static/dist/*.map

CMD npm run start:production
