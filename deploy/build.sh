set -e

export VERSION=pipelines-${BITBUCKET_BUILD_NUMBER}

mkdir -p build-output/test-reports
node -pe 'JSON.stringify({version:process.env.VERSION})' > build-output/release-version.json

cd ./deploy
docker-compose up --exit-code-from app --build
