import * as Sentry from '@sentry/browser'
import React from 'react'
import PropTypes from 'prop-types'
import config from './config'
import { Flag } from './services/JiraAP'

const { environment, sentryUrl, release } = config
const isDevelopment = environment === 'development'

export default class ErrorBoundary extends React.Component {
  static propTypes = {
    children: PropTypes.any
  }

  constructor (props) {
    super(props)

    if (!isDevelopment) {
      Sentry.init({
        dsn: sentryUrl,
        environment,
        release
      })
    }
  }

  componentDidCatch (error) {
    let errorId = 'DUMMY'

    if (!isDevelopment) {
      Sentry.captureException(error)
      errorId = Sentry.lastEventId()
    }

    Flag.error({
      body: [
        'Client-side error.',
        `Error ID: ${errorId}.`,
        'Please reload the page or try again later.'
      ].join(' ')
    })
  }

  render () {
    return this.props.children
  }
}
