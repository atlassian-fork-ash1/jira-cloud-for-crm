import React, { Component } from 'react'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import Button, { ButtonGroup } from '@atlaskit/button'
import Spinner from '@atlaskit/spinner'
import { FieldTextStateless } from '@atlaskit/field-text'
import Addon from '../../services/api/Addon'
import providers from '../../providers.json'

import './style.scss'

const provider = providers.find(crm => crm.id === 'dynamics')
const urlParams = new window.URLSearchParams(window.location.search)
const domain = urlParams.get('domain')
const addonToken = urlParams.get('addon-token')

@observer
export default class PreAuthPage extends Component {
  @observable domain = domain || ''
  @observable processing = false
  @observable isDisabled = !!domain

  connectAccount = async (e) => {
    if (e) {
      e.preventDefault()
    }

    this.processing = true
    const authUrl = await Addon.account.getCreateUrl({ provider: 'dynamics', addonToken })
    window.location.href = `${authUrl}&domain=${this.domain}`
  }

  close = () => {
    window.close()
  }

  componentDidMount () {
    if (this.domain) {
      this.connectAccount()
    }
  }

  render () {
    const spinner = this.processing ? <Spinner /> : null

    return (
      <div className='dynamics-pre-auth'>
        <img className='logo' src={provider.logo} />
        <h3>Allow Jira to access your Microsoft Dynamics CRM Account?</h3>
        <form onSubmit={this.connectAccount}>
          <div className='domain-form-content'>
            <p className='field-name'>Domain</p>
            <p className='field-description'>
              The domain of your CRM instance as you see it when
              you are logged in. This should look like:<br /><span>your-company.crm(optional-nr).dynamics.com</span>
            </p>
            <FieldTextStateless
              shouldFitContainer
              isLabelHidden
              label=''
              name='site'
              placeholder='your-company.crm.dynamics.com'
              onChange={e => (this.domain = e.target.value)}
              value={this.domain}
              disabled={this.isDisabled}
              autoFocus
            />
          </div>
          <ButtonGroup>
            <Button
              appearance='primary'
              iconAfter={spinner}
              type='submit'
              isDisabled={!this.domain || this.processing}
            >
              Yes, Continue
            </Button>
            <Button
              appearance='link'
              onClick={this.close}
            >
              Cancel
            </Button>
          </ButtonGroup>
        </form>
      </div>
    )
  }
}
