import React from 'react'
import PropTypes from 'prop-types'
import IssueRow from './IssueRow'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'react-lightning-design-system'

export default function IssuesTable ({ issues }) {
  return (
    <div>
      <Table bordered striped fixedLayout>
        <TableHeader>
          <TableRow>
            <TableHeaderColumn className='issue-key'>
              Issue Key
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-summary'>
              Issue Summary
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-type'>
              Type
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-status'>
              Status
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-priority'>
              Priority
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-reporter'>
              Reporter
            </TableHeaderColumn>
            <TableHeaderColumn className='issue-assignee'>
              Assignee
            </TableHeaderColumn>
          </TableRow>
        </TableHeader>
        <TableBody>
          {issues.length
            ? issues.map((issue, i) => (
              <IssueRow issue={issue} key={issue.key + i} />
            ))
            : <TableRow>
              <TableRowColumn colSpan='7' className='no-issues'>No issues were found</TableRowColumn>
            </TableRow>
          }
        </TableBody>
      </Table>
    </div>
  )
}

IssuesTable.propTypes = {
  issues: PropTypes.array.isRequired
}
