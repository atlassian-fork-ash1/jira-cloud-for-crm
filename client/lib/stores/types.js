import { shape, bool, string, object } from 'prop-types'
import { PropTypes } from 'mobx-react'

const { observableArray } = PropTypes

export const customersStoreType = shape({
  customersRegistry: observableArray.isRequired,
  isConnected: bool.isRequired,
  projectConfigUrl: string,
  isPreview: bool.isRequired,
  provider: object
})
