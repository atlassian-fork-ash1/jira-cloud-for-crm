import styled from 'styled-components'

import { N300 } from './colors'

export const ChildWrapper = styled.div`
  margin-left: -7px;
`

export const Header = styled.div`
  display: flex;
  justify-content: space-between;

  user-select: none;
  cursor: pointer;
`

export const TitleWrapper = styled.div`
  display: flex;
  align-items: center;

  font-size: 12px;
  font-weight: 500;
  color: ${N300};

  margin-bottom: 4px;
`
