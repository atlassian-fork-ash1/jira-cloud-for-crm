const blackList = {
  type: [
    'lookup',
    'uniqueidentifier',
  ],
  id: [
    'int_facebookservice',
    'int_kloutscoreservice',
    'int_twitterservice',
    'isautocreate',
    'parentcustomerid',
    'parentcustomeridtype',
    'subscriptionid',
  ],
  addressFields: [
    'id',
    '_addresstypecode',
    '_city',
    '_country',
    '_county',
    '_fax',
    '_freighttermscode',
    '_latitude',
    '_line1',
    '_line2',
    '_line3',
    '_longitude',
    '_name',
    '_postalcode',
    '_postofficebox',
    '_primarycontactname',
    '_shippingmethodcode',
    '_stateorprovince',
    '_telephone',
    '_upszone',
    '_utcoffset',
  ],
}

function filterFields (field) {
  const types = blackList.type.includes(field.type.toLowerCase())
  const ids = blackList.id.includes(field.id)
  const addressFields = blackList.addressFields
    .map(item => field.id.includes(item))
    .filter(item => item)

  return !(types || ids || addressFields.length)
}

module.exports = {
  async up (queryInterface) {
    const connections = await queryInterface.sequelize.query(
      `SELECT "Connections"."id", "options" FROM "Connections" INNER JOIN "Accounts" ON "Connections"."accountId" = "Accounts"."id" WHERE "Accounts"."type" = 'dynamics'`,
      { type: queryInterface.sequelize.QueryTypes.SELECT }
    )

    const promises = connections.map(async (connection) => {
      const groups = connection.options.schema.map((group) => {
        const fields = group.fields.filter(filterFields)

        return Object.assign({}, group, { fields })
      })

      await queryInterface.sequelize.query(
        `UPDATE "Connections" SET "options"='{"schema": ${JSON.stringify(groups)}}' WHERE "id"=${connection.id}`
      )
    })

    await Promise.all(promises)
  },
}
