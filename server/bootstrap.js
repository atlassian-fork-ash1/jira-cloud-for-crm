const { spawn, fork } = require('child_process')

const isProduction = process.env.NODE_ENV === 'production'
const isDebugMode = !!process.env.DEBUG
const microsGroup = process.env.MICROS_GROUP || 'WebServer'

if (isProduction) {
  start()
} else {
  startDev()
}

function start () {
  if (microsGroup === 'WebServer') {
    fork('./lib/web')
  } else {
    fork('./lib/worker')
  }
}

function startDev () {
  let processesLaunched = 0

  const launch = (module, envOverrides = {}) => {
    const args = [
      '--trace-warnings',
      isDebugMode && `--inspect=${5858 + processesLaunched}`,
      module,
      '--watch',
      './lib',
    ]

    spawn('nodemon', args.filter(Boolean), {
      stdio: 'inherit',
      env: { ...process.env, ...envOverrides },
    })

    processesLaunched++
  }

  launch('./lib/web.js')
  launch('./lib/worker.js', { MICROS_GROUP: 'GetIssues' })
  launch('./lib/worker.js', { MICROS_GROUP: 'SyncIssue' })
  launch('./lib/worker.js', { MICROS_GROUP: 'StHubIn' })
}
