const { handleError } = require('./util')

class Worker {
  constructor ({ logger, client, queue, handler }) {
    this.logger = logger
    this.client = client
    this.queue = queue
    this.handler = handler

    this._running = false
  }

  async _handleAndDeleteMessage (message) {
    try {
      await this.handler(message)

      const deleteParams = {
        QueueUrl: this.queue.url,
        ReceiptHandle: message.ReceiptHandle,
      }

      await this.client.deleteMessage(deleteParams).promise()
    } catch (error) /* istanbul ignore next */ {
      this.logger.error({ err: error }, 'Can\'t handle message')
    }
  }

  async _receiveMessages () {
    const params = {
      AttributeNames: [
        'SentTimestamp',
      ],
      MaxNumberOfMessages: 1,
      WaitTimeSeconds: 20,
      MessageAttributeNames: [
        'All',
      ],
      QueueUrl: this.queue.url,
    }

    const data = await this.client.receiveMessage(params).promise()

    return data.Messages || []
  }

  start () {
    this._running = true

    const loop = async () => {
      while (this._running) { // eslint-disable-line no-unmodified-loop-condition
        try {
          const messages = await this._receiveMessages()
          const handleAndDeleteMessage = this._handleAndDeleteMessage.bind(this)

          await Promise.all(messages.map(handleAndDeleteMessage))
        } catch (error) /* istanbul ignore next */ {
          handleError({
            error,
            msg: 'Can\'t receive messages',
            logger: this.logger,
          })
        }
      }
    }

    this._stopPromise = loop()
  }

  async stop () {
    this._running = false
    await this._stopPromise
  }
}

module.exports = Worker
