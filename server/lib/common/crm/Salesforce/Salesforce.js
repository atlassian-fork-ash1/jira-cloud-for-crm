const { get } = require('lodash')
const { format } = require('url')
const CRM = require('../CRM')
const CRMError = require('../Error')
const defaultSchema = require('./defaultSchema.json')
const blackList = require('./blackList.json')
const gas = require('../../gas')

class Salesforce extends CRM {
  async apiCheck () {
    try {
      await this.crmApi.getObjectFieldsMetadata('Contact')
    } catch (error) {
      throw error
    }
  }

  async getMyself () {
    const { name, email } = await this.crmApi.getMyself()

    return { name, email }
  }

  async getDefaultSchema () {
    const isLeadAvailable = await this._isLeadAvailable()
    const schema = this.defaultSchema.filter(group => (group.id === 'Lead' ? isLeadAvailable : true))
    const availableFields = await super.getFields(schema)

    return schema.map((group) => {
      const fields = group.fields.filter((field) => {
        const currentGroup = availableFields.find(({ id }) => group.id === id)

        return currentGroup.fields.some(({ id }) => field.id === id)
      })

      return { ...group, fields }
    })
  }

  async getParentFields (parentObjectId) {
    const data = await this.crmApi.getObjectFieldsMetadata(parentObjectId)

    return data.fields
      .filter(this._filterFields)
      .map((field) => {
        if (field.name === 'Name') {
          field.type = 'title'
        }

        if (field.name.includes('Address')) {
          field.type = 'address'
        }

        return {
          label: field.label,
          id: field.name,
          type: field.type,
        }
      })
  }

  async findCustomers (search, schema = this.defaultSchema) {
    const [contactList, leadList, accountList] = await Promise.all([
      this.crmApi.getContactList(search),
      this.crmApi.getLeadList(search),
      this.crmApi.getAccountList(search),
    ])

    const contacts = contactList.records.map(contact => ({
      recordType: 'Contact',
      recordId: contact.Id,
      name: contact.Name,
      email: contact.Email,
      account: contact.Account ? contact.Account.Name : '',
      photo: this.account.baseUrl + contact.PhotoUrl,
    }))

    const leads = leadList.records.map(lead => ({
      recordType: 'Lead',
      recordId: lead.Id,
      name: lead.Name,
      email: lead.Email,
      photo: this.account.baseUrl + lead.PhotoUrl,
    }))

    const accounts = accountList.records.map(account => ({
      recordType: 'Account',
      recordId: account.Id,
      name: account.Name,
      photo: this.account.baseUrl + account.PhotoUrl,
    }))

    const customers = [...contacts, ...leads, ...accounts]

    // In case if user didn't have permission on Lead object when created account
    // and then got it
    return !schema.find(g => g.id === 'Lead') && customers.some(c => c.recordType === 'Lead')
      ? customers.filter(c => c.recordType !== 'Lead')
      : customers
  }

  async findMatchContactId (email) {
    const response = await this.crmApi.getContacts({ email })

    return get(response, 'records.0.Id')
  }

  async getCustomerEmail (recordType, recordId) {
    if (recordType === 'Contact') {
      const response = await this.crmApi.getContacts({ id: recordId })

      return response.records[0].Email
    }

    if (recordType === 'Lead') {
      const response = await this.crmApi.getLeads({ id: recordId })

      return response.records[0].Email
    }
  }

  async getParentValues (schema, recordId, recordType) {
    const fields = this._mapSchemaFields(schema)

    const contactFields = fields.find(group => group.id === 'Contact').fields
    const accountFields = fields.find(group => group.id === 'Account').fields
    const leadFields = get(fields.find(group => group.id === 'Lead'), 'fields') // In case if Lead object is unavailable

    try {
      if (recordType === 'Contact') {
        const contacts = await this.crmApi.getContacts({ id: recordId }, [
          'Account.Id', 'Account.Name', ...contactFields, ...accountFields,
        ])

        return contacts.records[0]
      }

      if (recordType === 'Lead') {
        const leads = await this.crmApi.getLeads({ id: recordId }, leadFields)

        return leads.records[0]
      }

      if (recordType === 'Account') {
        const accounts = await this.crmApi.getAccounts({ id: recordId }, accountFields)

        return accounts.records[0]
      }
    } catch (error) {
      const errorBody = get(error, 'res.body[0]', {})
      const isInvalidFieldError = error.res.status === 400 && errorBody.errorCode === 'INVALID_FIELD'

      if (!isInvalidFieldError) throw error

      const field = this._extractDeletedField(errorBody.message)

      if (!field) throw error

      throw new CRMError('Field is deleted', {
        code: 'INVALID_FIELD',
        data: field,
      })
    }
  }

  async getChildValues (schema, parentRecordId, parentRecordType, childRecordType) {
    const fields = this._mapSchemaFields(schema)

    const methods = {
      Account: {
        Asset: 'getAssetsByAccount',
        Case: 'getCasesByAccount',
        Contact: 'getContactsByAccount',
        Entitlement: 'getEntitlementsByAccount',
        Opportunity: 'getOpportunitiesByAccount',
      },
      Contact: {
        Asset: 'getAssetsByContact',
        Case: 'getCasesByContact',
        Opportunity: 'getOpportunitiesByContact',
      },
    }

    const methodName = get(methods, `${parentRecordType}.${childRecordType}`)

    try {
      const response = await this.crmApi[methodName](parentRecordId, fields[0].fields)
      const [data] = response.records

      return data
    } catch (error) {
      // 'Entitlement' object type can be disabled in SF settings
      if (get(error, 'res.body.0.errorCode') === 'INVALID_TYPE' && childRecordType === 'Entitlement') {
        throw new CRMError(error.message, { code: 'OBJECT_DISABLED' })
      }

      throw error
    }
  }

  mapCustomerData (schema, data, isChild) {
    const fillGroupWithValues = (group, pluralName, relatedName) => {
      const items = get(data[pluralName], 'records', [])
      const values = items.map(record => mapGroupFields(group, record, relatedName))
      const url = items.map(record => getObjectUrl(record))

      return { ...group, values, url }
    }

    const mapGroupFields = (group, groupData, relatedName) => {
      if (!groupData) return []

      return group.fields.map((field) => {
        if (field.type === 'related_item' && relatedName && groupData[relatedName]) {
          groupData[field.id] = {
            name: groupData[relatedName].Name,
            url: getObjectUrl(groupData[relatedName]),
          }
        }

        if (field.type === 'address' && groupData[field.id]) {
          const { street, city, state, postalCode, country } = groupData[field.id]
          const address = [
            street,
            city,
            state,
            postalCode,
            country,
          ]

          groupData[field.id] = address.filter(el => el).join(', ')
        }

        return groupData[field.id]
      })
    }

    const getObjectUrl = (object) => {
      if (object) {
        return format({
          host: this.account.baseUrl,
          pathname: `/${object.Id}`,
        })
      }
    }

    // Mapping for top-level object
    if (!isChild) {
      return schema
        .map((group) => {
          if (group.id === 'Account' && data.Account) {
            data = data.Account
          }

          if (data.attributes.type !== group.id) return

          return {
            ...group,
            values: mapGroupFields(group, data),
            info: {
              recordId: data.Id,
              title: data.Name,
              url: getObjectUrl(data),
            },
          }
        })
        .filter(Boolean)
    }

    // Mapping for children
    return schema.map((group) => {
      let newGroup

      if (group.id === 'Opportunity') {
        // FIXME Temporary fix because of SF specific API
        let contactOpps

        if (data.OpportunityContactRoles) {
          contactOpps = data.OpportunityContactRoles.records.map(item => item.Opportunity)
        }

        const opportunities = contactOpps || get(data.Opportunities, 'records', [])
        const values = opportunities.map(record => mapGroupFields(group, record))
        const url = opportunities.map(record => getObjectUrl(record))

        newGroup = { ...group, values, url }
      }

      if (group.id === 'Contact') {
        newGroup = fillGroupWithValues(group, 'Contacts')
      }

      if (group.id === 'Case') {
        newGroup = fillGroupWithValues(group, 'Cases')
      }

      if (group.id === 'Asset') {
        newGroup = fillGroupWithValues(group, 'Assets', 'Product2')
      }

      if (group.id === 'Entitlement') {
        newGroup = fillGroupWithValues(group, 'Entitlements', 'Asset')
      }

      return newGroup
    })
  }

  formatJiraIssue (issue) {
    return {
      id: issue.id,
      key: issue.key,
      link: issue.self,
      summary: issue.fields.summary,
      type: issue.fields.issuetype,
      priority: issue.fields.priority,
      status: issue.fields.status,
      reporter: issue.fields.reporter,
      assignee: issue.fields.assignee,
    }
  }

  _mapSchemaFields (schema) {
    return schema.map(group => ({
      id: group.id,
      fields: group.fields
        .filter(field => field.id !== 'Name')
        .map(field => `${group.id}.${field.id}`),
    }))
  }

  _filterFields (field) {
    const types = blackList.type.includes(field.type)
    const ids = blackList.id.includes(field.name)
    const addressFields = blackList.addressFields
      .map(item => field.name.includes(item))
      .filter(item => item)

    return !(types || ids || addressFields.length)
  }

  async _isLeadAvailable () {
    try {
      await this.crmApi.getObjectFieldsMetadata('Lead')

      return true
    } catch (error) {
      // TODO: Remove it later. Just want to see how often it happens
      gas.send({
        name: 'lead_unavailable_on_connecting',
        cloudId: '-',
        user: '-',
      })

      return false
    }
  }

  _extractDeletedField (str) {
    /*
      Example of error message:
      "Name, Account.Id, Account.Name, Contact.ABC_Filed__c, Contact.Email
      ^
      ERROR at Row:1:Column:44
      No such column 'ABC_Filed__c' on entity 'Contact'. If you are attempting to use
      a custom field, be sure to append the '__c' after the custom field name.
      Please reference your WSDL or the describe call for the appropriate names.'"
    */

    const regex = /No such column '(.*?)' on entity '(.*?)'./
    const [, fieldId, objectId] = str.match(regex) || []

    if (fieldId && objectId) return { fieldId, objectId }
  }
}

Salesforce.defaultSchema = defaultSchema

module.exports = Salesforce
