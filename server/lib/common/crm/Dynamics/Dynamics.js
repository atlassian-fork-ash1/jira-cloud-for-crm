const { capitalize, get } = require('lodash')
const { format } = require('url')
const CRM = require('../CRM')
const CRMError = require('../Error')
const defaultSchema = require('./defaultSchema.json')
const defaultValues = require('./defaultValues.json')
const blackList = require('./blackList.json')

class Dynamics extends CRM {
  apiCheck () {
    return true
  }

  async getMyself () {
    const { fullname, internalemailaddress } = await this.crmApi.getMyself()

    return { name: fullname, email: internalemailaddress }
  }

  async getParentFields (parentObjectId) {
    const data = await this.crmApi.getObjectFieldsMetadata(parentObjectId)
    const fields = data.value[0].Attributes

    return fields
      .filter(this._filterFields)
      .map((field) => {
        const extractedField = {
          id: field.LogicalName,
          label: field.DisplayName.UserLocalizedLabel.Label,
          type: field.type || field.AttributeType.toLowerCase(),
        }

        if (extractedField.id === 'fullname') {
          extractedField.type = 'title'
        }

        return extractedField
      })
  }

  async findCustomers (search) {
    const [contactList, accountList] = await Promise.all([
      this.crmApi.getContacts({
        contains: {
          args: ['fullname', `'${search}'`],
        },
      }, ['fullname', 'emailaddress1', 'entityimage']),
      this.crmApi.getAccounts({
        contains: {
          args: ['name', `'${search}'`],
        },
      }, ['name', 'emailaddress1', 'entityimage']),
    ])

    return [...contactList, ...accountList].map(customer => ({
      recordType: customer.entityType,
      recordId: customer.id,
      name: customer.fullname || customer.name,
      email: customer.emailaddress1,
      account: '',
      photo: `data:image/jpeg;base64,${customer.entityimage}`,
    }))
  }

  async findMatchContactId (email) {
    const response = await this.crmApi.getContacts({ emailaddress1: email })

    return response[0] && response[0].contactid
  }

  async getParentValues (schema, recordId, recordType) {
    const fields = this._mapSchemaFields(schema)

    const contactFields = ['fullname', '_parentcustomerid_value', ...fields.find(group => group.id === 'Contact').fields]
    const accountFields = ['name', ...fields.find(group => group.id === 'Account').fields]

    try {
      if (recordType === 'Contact') {
        const contacts = await this.crmApi.getContacts({ contactid: recordId }, contactFields)
        const contact = contacts[0]

        if (contact && contact._parentcustomerid_value) {
          // load contact related account
          const [account] = await this.crmApi.getAccounts(
            { accountid: contact._parentcustomerid_value },
            accountFields
          )

          contact.account = account
        }

        return contact
      }

      if (recordType === 'Account') {
        const accounts = await this.crmApi.getAccounts({ accountid: recordId }, accountFields)

        return accounts[0]
      }
    } catch (error) {
      const errorBody = get(error, 'res.body.error', {})
      const isInvalidFieldError = (
        error.res.status === 400 &&
        get(errorBody, 'innererror.type') === 'Microsoft.OData.ODataException'
      )

      if (!isInvalidFieldError) throw error

      const field = this._extractDeletedField(errorBody.message)

      if (!field) throw error

      throw new CRMError('Field is deleted', {
        code: 'INVALID_FIELD',
        data: field,
      })
    }
  }

  async getChildValues (schema, parentRecordId, parentRecordType, childRecordType) {
    const fields = this._mapSchemaFields(schema)

    const methods = {
      Contact: {
        Incident: {
          name: 'getIncidents',
          searchBy: '_customerid_value',
        },
        Lead: {
          name: 'getLeads',
          searchBy: '_parentcontactid_value',
        },
        Opportunity: {
          name: 'getOpportunities',
          searchBy: '_parentcontactid_value',
        },
      },
      Account: {
        Incident: {
          name: 'getIncidents',
          searchBy: '_accountid_value',
        },
        Contact: {
          name: 'getContacts',
          searchBy: '_parentcustomerid_value',
        },
        Lead: {
          name: 'getLeads',
          searchBy: '_parentaccountid_value',
        },
        Opportunity: {
          name: 'getOpportunities',
          searchBy: '_parentaccountid_value',
        },
      },
    }

    const method = get(methods, `${parentRecordType}.${childRecordType}`)

    return this.crmApi[method.name]({ [method.searchBy]: parentRecordId }, fields[0].fields)
  }

  mapCustomerData (schema, data, isChild) {
    const mapGroupFields = (group, groupData) => (
      groupData
        ? group.fields.map(field => this._getFieldValue(group.id, groupData, field.id))
        : []
    )

    const getObjectUrl = (group, { id }) => format({
      host: this.account.baseUrl,
      pathname: '/main.aspx',
      query: {
        pagetype: 'entityrecord',
        etn: group.id.toLowerCase(),
        id,
      },
    })

    if (!isChild) {
      return schema
        .map((group) => {
          if (group.id === 'Account' && data.account) {
            data = data.account
          }

          if (group.id !== data.entityType) return

          return {
            ...group,
            values: mapGroupFields(group, data),
            info: {
              recordId: data.contactid || data.accountid || data.leadid,
              title: data.fullname || data.name,
              url: getObjectUrl(group, data),
            },
          }
        })
        .filter(Boolean)
    }

    return schema.map((group) => {
      group.values = data.map(record => mapGroupFields(group, record))
      group.url = data.map(record => getObjectUrl(group, record))

      return group
    })
  }

  _mapSchemaFields (schema) {
    return schema.map(group => ({
      id: group.id,
      fields: group.fields.map(field => field.id),
    }))
  }

  _filterFields (field) {
    if (!field.DisplayName.UserLocalizedLabel) return false

    const types = blackList.type.includes(field.type) ||
      blackList.type.includes(field.AttributeType.toLowerCase())
    const ids = blackList.id.includes(field.LogicalName)
    const addressFields = blackList.addressFields
      .map(item => field.LogicalName.includes(item))
      .filter(item => item)

    return field.DisplayName.UserLocalizedLabel && !(types || ids || addressFields.length)
  }

  _getFieldValue (groupId, groupData, fieldId) {
    // TODO Check if there's a better way to get names instead of code
    // Users can add their own fields' values so we get code that we can't resolve
    if (groupId === 'Incident') {
      const fieldsToMap = ['prioritycode', 'statecode', 'statuscode']

      if (fieldsToMap.includes(fieldId)) {
        return defaultValues[fieldId][groupData[fieldId]] || null
      }
    }

    return groupData[fieldId]
  }

  _extractDeletedField (str) {
    /*
      Example of error message:
      "Could not find a property
      named 'new_keyaccountmanager' on type 'Microsoft.Dynamics.CRM.account'."
    */

    const regex = /Could not find a property named '(.*?)' on type 'Microsoft.Dynamics.CRM.(.*?)'./
    const [, fieldId, objectId] = str.match(regex) || []

    if (fieldId && objectId) {
      return {
        fieldId,
        objectId: capitalize(objectId),
      }
    }
  }
}

Dynamics.defaultSchema = defaultSchema

module.exports = Dynamics
