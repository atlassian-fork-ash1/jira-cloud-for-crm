const { key, name, env, localBaseUrl } = require('./config').get('addon')

const getName = str => (env ? `${str} (${env})` : str)

module.exports = {
  key,
  name,
  description: 'CRM Integration (Salesforce, Microsoft Dynamics 365, and HubSpot) for Jira Cloud. Built and maintained by Atlassian',
  apiVersion: 1007,
  vendor: {
    name: 'Atlassian',
    url: 'https://www.atlassian.com',
  },
  baseUrl: localBaseUrl,
  links: {
    self: `${localBaseUrl}/api/addon/descriptor`,
    homepage: `${localBaseUrl}/api/addon/descriptor`,
  },
  authentication: {
    type: 'jwt',
  },
  lifecycle: {
    installed: '/api/instances/create',
    uninstalled: '/api/instances/delete',
  },
  scopes: [
    'READ',
    'WRITE',
    'PROJECT_ADMIN',
    'ACT_AS_USER',
    // Only production app is whitelisted for this scope
    !env && 'ACCESS_EMAIL_ADDRESSES',
  ].filter(Boolean),
  apiMigrations: {
    gdpr: true,
  },
  modules: {
    jiraProjectAdminTabPanels: [
      {
        key: 'crm-config-page',
        location: 'projectgroup4',
        name: {
          value: getName('CRM integration'),
        },
        weight: 999,
        url: '/project_config',
        cacheable: true,
        conditions: [
          {
            condition: 'user_is_project_admin',
          },
        ],
      },
    ],
    jiraIssueGlances: [
      {
        key: 'jira-crm-issue-panel-glance',
        icon: {
          width: 16,
          height: 16,
          url: '/img/crmforjira.svg',
        },
        content: {
          type: 'label',
          label: {
            value: 'Open linked customers',
          },
        },
        target: {
          type: 'web_panel',
          url: '/issue_panel',
        },
        name: {
          value: getName('CRM'),
        },
        cacheable: true,
        conditions: [
          {
            condition: 'entity_property_equal_to',
            params: {
              entity: 'project',
              propertyKey: key,
              value: 'true',
            },
          },
        ],
      },
    ],
    webPanels: [
      {
        key: 'jira-crm-issue-panel',
        location: 'atl.jira.view.issue.right.context',
        name: {
          value: getName('CRM'),
        },
        weight: 100,
        url: '/issue_panel',
        cacheable: true,
        conditions: [
          {
            condition: 'entity_property_equal_to',
            params: {
              entity: 'project',
              propertyKey: key,
              value: 'true',
            },
          },
        ],
      },
    ],
    generalPages: [
      {
        key: 'find-customer-dialog',
        location: 'none',
        name: {
          value: 'Match dialog',
        },
        url: '/match',
        cacheable: true,
      },
      {
        key: 'feedback-dialog',
        location: 'none',
        name: {
          value: 'Match dialog',
        },
        url: '/feedback',
        cacheable: true,
      },
    ],
    postInstallPage: {
      key: 'post-install-instructions',
      location: 'none',
      name: {
        value: 'Post Install Instructions',
      },
      url: '/post_install',
      cacheable: true,
    },
    webhooks: [{
      event: 'jira:issue_created',
      url: '/api/jira/issues/create',
    }, {
      event: 'jira:issue_updated',
      url: '/api/jira/issues/update',
    }, {
      event: 'jira:issue_deleted',
      url: '/api/jira/issues/delete',
    }, {
      event: 'project_deleted',
      url: '/api/jira/projects/delete',
    }],
  },
}
