module.exports = (sequelize, DataTypes) => {
  const Connection = sequelize.define('Connection', {
    instanceId: {
      type: DataTypes.STRING,
      allowNull: false,
      references: {
        model: 'Instances',
        key: 'id',
      },
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    accountId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Accounts',
        key: 'id',
      },
      onUpdate: 'cascade',
      onDelete: 'cascade',
    },
    projectId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    options: {
      type: DataTypes.JSONB,
      allowNull: false,
    },
    totalIssues: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    checkedIssues: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
    matchedIssues: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
    },
  }, {
    indexes: [
      { fields: ['instanceId'] },
      { fields: ['accountId'] },
      { fields: ['projectId'] },
      { fields: ['createdAt'] },
    ],
  })

  Connection.associate = function associate (models) {
    this.belongsTo(models.Instance, { foreignKey: 'instanceId' })
    this.belongsTo(models.Account, { foreignKey: 'accountId' })
    this.Matches = this.hasMany(models.Match, {
      foreignKey: 'connectionId',
      onDelete: 'cascade',
    })
  }

  return Connection
}
