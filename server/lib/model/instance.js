// Jira instance

module.exports = (sequelize, DataTypes) => {
  const Instance = sequelize.define('Instance', {
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    baseUrl: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    },
    sharedSecret: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    oauthClientId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    serverVersion: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    pluginsVersion: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    indexes: [
      { fields: ['baseUrl'] },
      { fields: ['serverVersion'] },
      { fields: ['pluginsVersion'] },
      { fields: ['createdAt'] },
    ],
  })

  Instance.associate = function associate (models) {
    this.Accounts = this.hasMany(models.Account, {
      foreignKey: 'instanceId',
      onDelete: 'cascade',
    })
    this.Connections = this.hasMany(models.Connection, {
      foreignKey: 'instanceId',
      onDelete: 'cascade',
    })
  }

  Instance.prototype.userToken = function userToken (userKey, token, expiration) {
    const id = ['cache', 'Token', this.id, userKey.replace(/\s/g, '+')].join(':')
    const { cache } = sequelize

    if (token) {
      return cache.setAsync(id, token, expiration)
    }

    return cache.getAsync(id)
  }

  Instance.prototype.userPermission = async function userPermission (userKey, permission) {
    const id = ['cache', 'Permission', this.id, userKey.replace(/\s/g, '+')].join(':')
    const { cache } = sequelize
    const expiration = 60

    if (permission) {
      return cache.setAsync(id, permission, expiration)
    }

    return cache.getAsync(id)
  }

  return Instance
}
