const Service = require('../Service')
const ServiceError = require('../Error')
const { getProjectAdminPermission } = require('./util')

const { schema } = require('./validatorRules')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'positive_integer'],
      options: { nested_object: {
        schema,
      } },
    }

    return this.validator(params, rules)
  }

  async execute ({ id, options }) {
    const connection = await this.model.Connection.findByPk(id, {
      include: [{
        model: this.model.Instance,
        where: { id: this.context('instance.id') },
      }, {
        model: this.model.Account,
        required: true,
      }],
    })

    if (!connection) {
      throw new ServiceError('Wrong connection id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    const hasProjectAdminPermission = await getProjectAdminPermission(
      this.context(),
      connection.projectId
    )

    if (!hasProjectAdminPermission) {
      throw new ServiceError('You have no access to project related to this connection', {
        code: 'PERMISSION_DENIED',
      })
    }

    await connection.update({ options })

    this.logger.info({ id }, 'Connection was updated')

    return {
      id: connection.id,
      accountId: connection.accountId,
      projectId: connection.projectId,
      options: connection.options,
    }
  }
}
