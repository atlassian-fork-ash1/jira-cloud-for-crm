const Service = require('../../Service')
const ServiceError = require('../../Error')

const { formatAccount, formatInstance, formatConnection } = require('../utils')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ id }) {
    const instance = await this.model.Instance.findByPk(id, {
      paranoid: false,
    })

    if (!instance) {
      throw new ServiceError('Invalid instance id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    const jira = this.api('Jira', instance)

    const accounts = await this.model.Account.findAll({
      include: {
        model: this.model.Connection,
        where: { instanceId: id },
      },
    })

    const instanceData = formatInstance(instance)

    try {
      const user = await jira.getUser()

      instanceData.status = user.active ? 'active' : 'inactive'
    } catch (error) {
      instanceData.status = 'inactive'
      instanceData.error = error
    }

    instanceData.accounts = accounts.map((account) => {
      const accountData = formatAccount(account)

      accountData.connections = account.Connections.map(formatConnection)

      return accountData
    })

    return instanceData
  }
}
