const { sortBy } = require('lodash')
const Service = require('../Service')
const ServiceError = require('../Error')
const getCRMClass = require('../../common/crm')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      projectId: ['required', 'string'],
      issueId: ['required', 'string'],
      search: ['string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ projectId, issueId, search = '' }) {
    const connection = await this.model.Connection.findOne({
      include: [{
        model: this.model.Match,
        where: { issueId },
        required: false,
      }, {
        model: this.model.Account,
        where: {
          instanceId: this.context('instance.id'),
        },
      }],
      where: { projectId },
    })

    if (!connection) {
      throw new ServiceError('No connection for current project', {
        code: 'NO_CONNECTION',
      })
    }

    const account = connection.Account

    if (!account.isAuthorized) {
      throw new ServiceError('CRM account needs authorization', {
        code: 'INVALID_CLIENT',
      })
    }

    const CRM = getCRMClass(account.type)
    const crm = new CRM(this.api.bind(this), account)

    try {
      const customers = await crm.findCustomers(search, connection.options.schema)

      const isMatchExists = customer => connection.Matches.some(
        m => m.recordId === customer.recordId && m.recordType === customer.recordType
      )

      return sortBy(
        customers.filter(c => !isMatchExists(c)),
        'name'
      )
    } catch (error) {
      if (error.code === 'CRM_ACCOUNT_INACTIVE') {
        account.deactivate()
        this.logger.info({ err: error, accountId: account.id }, 'Account is deactivated')
      }

      if (['CRM_ACCOUNT_INACTIVE', 'INVALID_CLIENT'].includes(error.code)) {
        throw new ServiceError('CRM account needs authorization', {
          code: error.code,
        })
      }

      throw error
    }
  }
}
