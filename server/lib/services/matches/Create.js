const Service = require('../Service')
const ServiceError = require('../Error')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      projectId: ['required', 'string'],
      issueId: ['required', 'string'],
      recordType: ['required', 'string'],
      recordId: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ projectId, issueId, recordType, recordId }) {
    const connection = await this.model.Connection.findOne({
      where: {
        instanceId: this.context('instance.id'),
        projectId,
      },
    })

    if (!connection) {
      throw new ServiceError('Wrong project id', {
        code: 'WRONG_ID',
        fields: ['projectId'],
      })
    }

    const [match] = await this.model.Match.upsert({
      connectionId: connection.id,
      issueId,
      recordType,
      recordId,
      isManual: true,
    }, { returning: true })

    return {
      id: match.id,
      recordType: match.recordType,
      recordId: match.recordId,
    }
  }
}
