const Route = require('../Route')

module.exports = class extends Route {
  delete (req, res) {
    this.initService('hooks.projects.Delete', req.body).run(req, res)
  }
}
