const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts],
  context.model
))

describe('Instances.Delete', () => {
  test('Delete Jira instance', async () => {
    const { clientKey: instanceId } = mocks.jira.hooks.instanceCreate

    const jiraToken = utils.auth.jira()

    await context.client
      .post('/api/instances/delete')
      .set('Authorization', jiraToken)
      .expect(201)

    const instance = await context.model.Instance.findByPk(instanceId)
    const accounts = await context.model.Account.findAll({ where: { instanceId } })

    expect(instance).toBe(null)
    expect(accounts).toHaveLength(0)
  })
})
