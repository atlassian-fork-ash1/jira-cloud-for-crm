const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')
const salesforceSchema = require('../../lib/common/crm/Salesforce/defaultSchema.json')

const context = new Context({ app: true, worker: true })

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts],
  context.model
))

beforeEach(async () => {
  const connections = await context.model.Connection.findAll()

  await Promise.all(connections
    .map(({ id }) => context.model.sequelize.cache.delAsync(`sync:${id}:test@atlassian.com`)))
})

const sleep = ms => new Promise((resolve) => {
  const timeout = setTimeout(() => {
    resolve()
    clearTimeout(timeout)
  }, ms)
})

async function waitForIsDone (scope) {
  while (!scope.isDone()) {
    await sleep(500)
  }
}

describe('Connections.Create', () => {
  test('New connection', async () => {
    const projectId = '1000'
    const accountId = Number(fixtures.accounts[0].data.id)

    const apiCalls = [
      mocks.jira.listeners.searchIssues(),
      mocks.jira.listeners.getUserEmail(),
      mocks.jira.listeners.setProjectProperty(),
      mocks.salesforce.listeners.getContactFieldsMetadata(),
      mocks.salesforce.listeners.getAccountFieldsMetadata(),
      mocks.salesforce.listeners.getLeadFieldsMetadata(),
      mocks.salesforce.listeners.getLeadFieldsMetadata(),
      mocks.salesforce.listeners.getContactByEmail('test@atlassian.com'),
      mocks.salesforce.listeners.getContactByEmail('test@mail.com'),
    ]

    const { body } = await context.client
      .post('/api/connections')
      .send({ accountId, projectId })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    await Promise.all(apiCalls.map(waitForIsDone))

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountId).toBe(accountId)
    expect(body.data.projectId).toBe(projectId)
    expect(body.data.options).toEqual({
      schema: salesforceSchema,
    })
  })

  test('Create connection when one is already exists', async () => {
    const projectId = '1000'
    const accountId = Number(fixtures.accounts[0].data.id)

    const apiCalls = [
      mocks.jira.listeners.setProjectProperty(),
    ]

    const { body } = await context.client
      .post('/api/connections')
      .send({ accountId, projectId })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    await Promise.all(apiCalls.map(waitForIsDone))

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountId).toBe(accountId)
    expect(body.data.projectId).toBe(projectId)
    expect(body.data.options).toEqual({
      schema: salesforceSchema,
    })
  })

  test('New connection when Salesforce Lead object is disabled', async () => {
    const projectId = '1000'
    const accountId = Number(fixtures.accounts[0].data.id)

    const apiCalls = [
      mocks.jira.listeners.searchIssues(),
      mocks.jira.listeners.getUserEmail('test@atlassian.com'),
      mocks.jira.listeners.setProjectProperty(),
      mocks.salesforce.listeners.getContactFieldsMetadata(),
      mocks.salesforce.listeners.getAccountFieldsMetadata(),
      mocks.salesforce.listeners.getLeadFieldsMetadataNotFound(),
      mocks.salesforce.listeners.getContactByEmail('test@atlassian.com'),
    ]

    await context.model.Connection.destroy({
      where: { accountId, projectId },
    })

    const { body } = await context.client
      .post('/api/connections')
      .send({ accountId, projectId })
      .set('Authorization', utils.auth.jira())
      .expect(201)

    await Promise.all(apiCalls.map(waitForIsDone))

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data.accountId).toBe(accountId)
    expect(body.data.projectId).toBe(projectId)
    expect(body.data.options).toEqual({
      schema: salesforceSchema.filter(object => object.id !== 'Lead'),
    })
  })

  test('Create connection with no account', async () => {
    const projectId = '1000'
    const accountId = 111

    const { body } = await context.client
      .post('/api/connections')
      .send({ accountId, projectId })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error).toEqual({
      message: 'Wrong account id',
      code: 'WRONG_ID',
      fields: ['accountId'],
    })
  })

  test.skip('New connection without admin permissions', async () => {
    mocks.jira.listeners.getBearerToken()
    mocks.jira.listeners.getProjectPermissions({
      permissions: {
        ADMINISTER_PROJECTS: { havePermission: false },
        PROJECT_ADMIN: { havePermission: false },
      },
    })
    mocks.salesforce.listeners.getContactFieldsMetadata()
    mocks.salesforce.listeners.getAccountFieldsMetadata()
    mocks.salesforce.listeners.getLeadFieldsMetadataNotFound()

    mocks.jira.listeners.setProjectProperty()

    const projectId = '1000'
    const accountId = Number(fixtures.accounts[0].data.id)

    const authToken = utils.auth.jira({
      jiraAccountId: 'not-admin',
    })

    const { body } = await context.client
      .post('/api/connections')
      .send({ accountId, projectId })
      .set('Authorization', authToken)
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error.code).toBe('PERMISSION_DENIED')
  })
})
