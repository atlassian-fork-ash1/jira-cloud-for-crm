const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections, ...fixtures.matches],
  context.model
))

describe('Matches.Create', () => {
  test('Delete match', async () => {
    const { body } = await context.client
      .delete('/api/matches')
      .send({ projectId: '1000', matchId: 1 })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
  })

  test('No connection was found', async () => {
    const { body } = await context.client
      .delete('/api/matches')
      .send({ projectId: '1111', matchId: 1 })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error).toEqual({
      message: 'Wrong project id',
      code: 'WRONG_ID',
      fields: ['projectId'],
    })
  })
})
